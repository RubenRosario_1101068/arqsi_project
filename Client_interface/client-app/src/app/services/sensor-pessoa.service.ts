import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SensorPessoa } from '../interfaces/sensor-pessoa.interface';
import { Observable } from 'rxjs';
import * as moment from 'moment/moment';

@Injectable({
  providedIn: 'root'
})
export class SensorPessoaService {
  URL_GET = 'https://arqsiespecialservice2-qqodg26nna-ew.a.run.app/api/';
  URL_SET = 'https://webserviceit1.azurewebsites.net/api/';

  constructor(private http: HttpClient) {}

  getListagemSensorPessoa(
    registoId?: number,
    SensorId?: number,
    dataHoraInic?: Date,
    dataHoraFim?: Date,
    entradas?: number,
    saidas?: number
  ) {
    let result: Observable<any>;
    if (registoId || false) {
      result = this.http.get(
        this.URL_GET + 'sensorPessoa/registo/' + registoId
      );
    } else if (SensorId || false) {
      result = this.http.get(this.URL_GET + 'sensorPessoa/' + SensorId);
    } else if (entradas || false) {
      result = this.http.get(
        this.URL_GET + 'sensorPessoa/entrada_superior/' + entradas
      );
    } else {
      result = this.http.get(this.URL_GET + 'sensorPessoa/');
    }
    return result;
  }

  setSensorPessoa(sensorPessoa: SensorPessoa) {
    let result: Observable<any>;
    console.log(sensorPessoa);
    // fazer um create
    if (typeof sensorPessoa.id === 'undefined') {
      const body = {
        SensorId: sensorPessoa.SensorId + '',
        RecintoId: sensorPessoa.RecintoId + '',
        Data_Hora: moment.parseZone(
          sensorPessoa.Data_Hora,
          'YYYY-MM-DD HH:mm:ss'
        ),
        Entrada: sensorPessoa.Entrada + '',
        Saida: sensorPessoa.Saida + ''
      };
      result = this.http.post<any>(this.URL_SET + 'sensorPessoa/', body);
    } else {
      // fazer um update
      const body = {
        id: sensorPessoa.id + '',
        SensorId: sensorPessoa.SensorId + '',
        RecintoId: sensorPessoa.RecintoId + '',
        Data_Hora: sensorPessoa.Data_Hora + '',
        Entrada: sensorPessoa.Entrada + '',
        Saida: sensorPessoa.Saida + ''
      };
      result = this.http.put<any>(
        this.URL_SET + 'sensorPessoa/' + sensorPessoa.id,
        body
      );
    }
    return result;
  }

  deleteSensorPessoa(idSensor) {
    return this.http.delete<any>(this.URL_SET + 'sensorPessoa/' + idSensor);
  }
}
