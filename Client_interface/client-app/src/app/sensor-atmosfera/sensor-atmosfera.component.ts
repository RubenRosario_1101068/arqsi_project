import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { SensorPessoaService } from '../services/sensor-pessoa.service';
import { SensorAtmosfera } from './../interfaces/sensor-atmosfera.interface';
import { MatSnackBar } from '@angular/material';
import { SensorAtmosferaService } from '../services/sensor-atmosfera.service';

@Component({
  selector: 'app-sensor-atmosfera',
  templateUrl: './sensor-atmosfera.component.html',
  styleUrls: ['./sensor-atmosfera.component.css']
})
export class SensorAtmosferaComponent implements OnInit {
  formularioPesquisa: FormGroup;
  formularioEdicao: FormGroup;
  listaSensores: SensorAtmosfera[];
  linhaSelecionada = -1;
  nomeColunas: string[] = [
    'id',
    'SensorId',
    'RecintoId',
    'Data_Hora',
    'Temperatura',
    'Co2',
    'Humidade',
    'Pluviosidade'
  ];

  constructor(
    private formBuilder: FormBuilder,
    private getSensorService: SensorAtmosferaService,
    private snackBar: MatSnackBar
  ) {
    this.listaSensores = [];
  }

  ngOnInit() {
    this.inicializarFormPesquisa();
    this.inicializarFormEdicao();
  }

  inicializarFormPesquisa() {
    this.formularioPesquisa = this.formBuilder.group({
      idRegisto: ['', [Validators.pattern('^[0-9]*$')]],
      idSensor: ['', [Validators.pattern('^[0-9]*$')]],
      idRecinto: ['', [Validators.pattern('^[0-9]*$')]],
      dataHoraInic: new FormControl(),
      dataHoraFim: new FormControl(),
      temperatura: ['', [Validators.pattern('^[0-9]*$')]],
      co2: ['', [Validators.pattern('^[0-9]*$')]],
      humidade: ['', [Validators.pattern('^[0-9]*$')]],
      pluviosidade: ['', [Validators.pattern('^[0-9]*$')]]
    });
  }

  inicializarFormEdicao() {
    this.formularioEdicao = this.formBuilder.group({
      idSensor: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      idRecinto: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      dataHora: new FormControl(),
      temperatura: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      co2: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      humidade: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      pluviosidade: ['', [Validators.pattern('^[0-9]*$'), Validators.required]]
    });
  }

  editarLinha(linha) {
    if (this.linhaSelecionada === linha.id) {
      this.linhaSelecionada = -1;
      // set valores no formulário de edição
      this.formularioEdicao.setValue({
        idSensor: '',
        idRecinto: '',
        dataHora: '',
        temperatura: '',
        co2: '',
        humidade: '',
        pluviosidade: ''
      });
    } else {
      this.linhaSelecionada = linha.id;
      // set valores no formulário de edição
      this.formularioEdicao.setValue({
        idSensor: linha.SensorId,
        idRecinto: linha.RecintoId,
        dataHora: linha.Data_Hora,
        temperatura: linha.Temperatura,
        co2: linha.Co2,
        humidade: linha.Humidade,
        pluviosidade: linha.Pluviosidade
      });
    }
  }

  submit() {
    const idRegisto = this.formularioPesquisa.get('idRegisto').value;
    const idSensor = this.formularioPesquisa.get('idSensor').value;
    const idRecinto = this.formularioPesquisa.get('idRecinto').value;
    const dataHoraInic = this.formularioPesquisa.get('dataHoraInic').value;
    const dataHoraFim = this.formularioPesquisa.get('dataHoraFim').value;
    const temperatura = this.formularioPesquisa.get('temperatura').value;
    const co2 = this.formularioPesquisa.get('co2').value;
    const humidade = this.formularioPesquisa.get('humidade').value;
    const pluviosidade = this.formularioPesquisa.get('pluviosidade').value;

    this.getSensorService
      .getListagemSensorAtmosfera(
        idRegisto,
        idSensor,
        idRecinto,
        dataHoraInic,
        dataHoraFim,
        temperatura,
        co2,
        humidade,
        pluviosidade
      )
      .subscribe((data: SensorAtmosfera[]) => {
        if (data.length === 0) {
          this.snackBar.open('não existem dados', 'fechar', {
            duration: 5000,
            verticalPosition: 'top'
          });
        } else {
          this.listaSensores = data;
          this.snackBar.open(
            'foram extraído(s) ' + data.length + ' resultado(s)',
            'fechar',
            {
              duration: 5000,
              verticalPosition: 'top'
            }
          );
        }
      });
  }

  guardarSensor() {
    let sensor: SensorAtmosfera;
    // Criar um novo
    if (this.linhaSelecionada === -1) {
      sensor = {
        SensorId: this.formularioEdicao.get('idSensor').value,
        RecintoId: this.formularioEdicao.get('idRecinto').value,
        Data_Hora: this.formularioEdicao.get('dataHora').value,
        Temperatura: this.formularioEdicao.get('temperatura').value,
        Co2: this.formularioEdicao.get('co2').value,
        Humidade: this.formularioEdicao.get('humidade').value,
        Pluviosidade: this.formularioEdicao.get('pluviosidade').value
      };
    } else {
      // editar o existente
      sensor = {
        id: this.linhaSelecionada,
        SensorId: this.formularioEdicao.get('idSensor').value,
        RecintoId: this.formularioEdicao.get('idRecinto').value,
        Data_Hora: this.formularioEdicao.get('dataHora').value,
        Temperatura: this.formularioEdicao.get('temperatura').value,
        Co2: this.formularioEdicao.get('co2').value,
        Humidade: this.formularioEdicao.get('humidade').value,
        Pluviosidade: this.formularioEdicao.get('pluviosidade').value
      };
    }

    this.getSensorService.setSensorAtmosfera(sensor).subscribe(data => {
      if (data) {
        this.snackBar.open('Registo guardado com sucesso', 'fechar', {
          duration: 5000,
          verticalPosition: 'top'
        });
      }
    });
  }

  eliminarLinha() {
    this.getSensorService
      .deleteSensorAtmosfera(this.linhaSelecionada)
      .subscribe(data => {
        if (data) {
          this.snackBar.open('Registo eliminado com sucesso', 'fechar', {
            duration: 5000,
            verticalPosition: 'top'
          });
          this.inicializarFormEdicao();
          this.linhaSelecionada = -1;
        }
      });
  }
}
