import { SensorAtmosfera } from './../interfaces/sensor-atmosfera.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment/moment';

@Injectable({
  providedIn: 'root'
})
export class SensorAtmosferaService {
  URL_GET = 'https://arqsiespecialservice2-qqodg26nna-ew.a.run.app/api/';
  URL_SET = 'https://webserviceit1.azurewebsites.net/api/';

  constructor(private http: HttpClient) {}

  getListagemSensorAtmosfera(
    registoId?: number,
    SensorId?: number,
    RecintoId?: number,
    dataHoraInic?: Date,
    dataHoraFim?: Date,
    Temperatura?: number,
    Co2?: number,
    Humidade?: number,
    Pluviosidade?: number
  ) {
    let result: Observable<any>;
    if (registoId || false) {
      result = this.http.get(
        this.URL_GET + 'sensorAtmosfera/registo/' + registoId
      );
    } else if (SensorId || false) {
      result = this.http.get(this.URL_GET + 'sensorAtmosfera/' + SensorId);
    } else if (Temperatura || false) {
      result = this.http.get(
        this.URL_GET + 'sensorAtmosfera/temperatura_superior/' + Temperatura
      );
    } else {
      result = this.http.get(this.URL_GET + 'sensorAtmosfera/');
    }
    return result;
  }

  setSensorAtmosfera(sensorAtmosfera: SensorAtmosfera) {
    let result: Observable<any>;
    // fazer um create
    if (typeof sensorAtmosfera.id === 'undefined') {
      const body = {
        SensorId: sensorAtmosfera.SensorId + '',
        RecintoId: sensorAtmosfera.RecintoId + '',
        Data_Hora: moment.parseZone(
          sensorAtmosfera.Data_Hora,
          'YYYY-MM-DD HH:mm:ss'
        ),
        Temperatura: sensorAtmosfera.Temperatura + '',
        Co2: sensorAtmosfera.Co2 + '',
        Humidade: sensorAtmosfera.Humidade + '',
        Pluviosidade: sensorAtmosfera.Pluviosidade + ''
      };
      result = this.http.post<any>(this.URL_SET + 'sensorAtmosfera/', body);
    } else {
      // fazer um update
      const body = {
        id: sensorAtmosfera.id + '',
        SensorId: sensorAtmosfera.SensorId + '',
        RecintoId: sensorAtmosfera.RecintoId + '',
        Data_Hora: sensorAtmosfera.Data_Hora + '',
        Temperatura: sensorAtmosfera.Temperatura + '',
        Co2: sensorAtmosfera.Co2 + '',
        Humidade: sensorAtmosfera.Humidade + '',
        Pluviosidade: sensorAtmosfera.Pluviosidade + ''
      };
      result = this.http.put<any>(
        this.URL_SET + 'sensorAtmosfera/' + sensorAtmosfera.id,
        body
      );
    }
    return result;
  }

  deleteSensorAtmosfera(idSensor) {
    return this.http.delete<any>(this.URL_SET + 'sensorAtmosfera/' + idSensor);
  }
}
