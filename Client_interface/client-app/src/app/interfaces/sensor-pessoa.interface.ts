export interface SensorPessoa {
  id?: number;
  SensorId: number;
  RecintoId: number;
  Data_Hora: Date;
  Entrada: number;
  Saida: number;
}
