import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorPessoaComponent } from './sensor-pessoa.component';

describe('SensorPessoaComponent', () => {
  let component: SensorPessoaComponent;
  let fixture: ComponentFixture<SensorPessoaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorPessoaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorPessoaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
