
using IT1.Models;
using System;

namespace IT1.DTO
{
   public class SensorPessoaDTO
    {
        public int id { get; set;}
        public int sensorId {get; set;}
        public int recintoId {get; set;}
        public DateTime data_Hora{get; set;}
        public int entrada {get; set;}
        public int saida {get; set;}
        
        public SensorPessoaDTO()
        {
        }

        public SensorPessoaDTO(SensorPessoa sensorpessoa){
            this.id = sensorpessoa.Id;
            this.sensorId = sensorpessoa.SensorId;
            this.recintoId = sensorpessoa.RecintoId;
            this.data_Hora = sensorpessoa.Data_Hora;
            this.entrada = sensorpessoa.Entrada;
            this.saida = sensorpessoa.Saida;
        }

        public SensorPessoa fromDTO(){
            var p = new SensorPessoa();
            p.Id = this.id;
            p.SensorId = this.sensorId;
            p.RecintoId = this.recintoId;
            p.Data_Hora = this.data_Hora;
            p.Entrada = this.entrada;
            p.Saida = this.saida;
            return p;
        }
    }
}