using IT1.Models;
using System;

namespace IT1.DTO
{
   public class SensorAtmosferaDTO
    {
        public int id { get; set;}
        public int sensorId {get; set;}
        public int recintoId {get; set;}
        public DateTime data_Hora{get; set;}       
        public float temperatura {get; set;}        
        public float co2 {get; set;}
        public float humidade {get; set;}
        public float pluviosidade {get; set;}    

        public SensorAtmosferaDTO()
        {
        }

        public SensorAtmosferaDTO(SensorAtmosfera sensoratmosfera){
            this.id = sensoratmosfera.Id;
            this.sensorId = sensoratmosfera.SensorId;
            this.recintoId = sensoratmosfera.RecintoId;
            this.data_Hora = sensoratmosfera.Data_Hora;
            this.temperatura = sensoratmosfera.Temperatura;
            this.co2 = sensoratmosfera.Co2;
            this.humidade = sensoratmosfera.Humidade;
            this.pluviosidade = sensoratmosfera.Pluviosidade;
        }

        public SensorAtmosfera fromDTO(){
            var p = new SensorAtmosfera();
            p.Id = this.id;
            p.SensorId = this.sensorId;
            p.RecintoId = this.recintoId;
            p.Data_Hora = this.data_Hora;
            p.Temperatura = this.temperatura;
            p.Co2 = this.co2;
            p.Humidade = this.humidade;
            p.Pluviosidade = this.pluviosidade;
            return p;
        }
    }
}