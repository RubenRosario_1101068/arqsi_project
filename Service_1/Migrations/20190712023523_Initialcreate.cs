﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IT1.Migrations
{
    public partial class Initialcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SensorAtmosfera",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SensorId = table.Column<int>(nullable: false),
                    RecintoId = table.Column<int>(nullable: false),
                    Data_Hora = table.Column<DateTime>(nullable: false),
                    Temperatura = table.Column<float>(nullable: false),
                    Co2 = table.Column<float>(nullable: false),
                    Humidade = table.Column<float>(nullable: false),
                    Pluviosidade = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SensorAtmosfera", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SensorPessoa",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SensorId = table.Column<int>(nullable: false),
                    RecintoId = table.Column<int>(nullable: false),
                    Data_Hora = table.Column<DateTime>(nullable: false),
                    Entrada = table.Column<int>(nullable: false),
                    Saida = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SensorPessoa", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "SensorAtmosfera",
                columns: new[] { "Id", "Co2", "Data_Hora", "Humidade", "Pluviosidade", "RecintoId", "SensorId", "Temperatura" },
                values: new object[,]
                {
                    { 1, 3f, new DateTime(2019, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), 9f, 6f, 1, 1, 10f },
                    { 23, 3f, new DateTime(2019, 5, 7, 0, 22, 0, 0, DateTimeKind.Unspecified), 5f, 6f, 1, 1, 4f },
                    { 21, 2f, new DateTime(2019, 5, 7, 0, 20, 0, 0, DateTimeKind.Unspecified), 11f, 3f, 1, 1, 5f },
                    { 20, 4f, new DateTime(2019, 5, 7, 0, 19, 0, 0, DateTimeKind.Unspecified), 10f, 6f, 1, 1, 5f },
                    { 19, 3f, new DateTime(2019, 5, 7, 0, 18, 0, 0, DateTimeKind.Unspecified), 5f, 4f, 1, 1, 6f },
                    { 18, 4f, new DateTime(2019, 5, 7, 0, 17, 0, 0, DateTimeKind.Unspecified), 8f, 0f, 1, 1, 7f },
                    { 17, 2f, new DateTime(2019, 5, 7, 0, 16, 0, 0, DateTimeKind.Unspecified), 9f, 5f, 1, 1, 8f },
                    { 16, 2f, new DateTime(2019, 5, 7, 0, 15, 0, 0, DateTimeKind.Unspecified), 7f, 0f, 1, 1, 9f },
                    { 15, 3f, new DateTime(2019, 5, 7, 0, 14, 0, 0, DateTimeKind.Unspecified), 6f, 23f, 1, 1, 10f },
                    { 14, 4f, new DateTime(2019, 5, 7, 0, 13, 0, 0, DateTimeKind.Unspecified), 7f, 5f, 1, 1, 10f },
                    { 13, 1f, new DateTime(2019, 5, 7, 0, 12, 0, 0, DateTimeKind.Unspecified), 9f, 6f, 1, 1, 10f },
                    { 22, 4f, new DateTime(2019, 5, 7, 0, 21, 0, 0, DateTimeKind.Unspecified), 15f, 4f, 1, 1, 5f },
                    { 11, 7f, new DateTime(2019, 5, 7, 0, 10, 0, 0, DateTimeKind.Unspecified), 1f, 8f, 1, 1, 11f },
                    { 10, 8f, new DateTime(2019, 5, 7, 0, 9, 0, 0, DateTimeKind.Unspecified), 2f, 5f, 1, 1, 10f },
                    { 9, 8f, new DateTime(2019, 5, 7, 0, 8, 0, 0, DateTimeKind.Unspecified), 4f, 6f, 1, 1, 7f },
                    { 8, 9f, new DateTime(2019, 5, 7, 0, 7, 0, 0, DateTimeKind.Unspecified), 7f, 4f, 1, 1, 6f },
                    { 7, 5f, new DateTime(2019, 5, 7, 0, 6, 0, 0, DateTimeKind.Unspecified), 8f, 5f, 1, 1, 7f },
                    { 6, 5f, new DateTime(2019, 5, 7, 0, 5, 0, 0, DateTimeKind.Unspecified), 9f, 4f, 1, 1, 9f },
                    { 5, 4f, new DateTime(2019, 5, 7, 0, 4, 0, 0, DateTimeKind.Unspecified), 9f, 3f, 1, 1, 20f },
                    { 4, 4f, new DateTime(2019, 5, 7, 0, 3, 0, 0, DateTimeKind.Unspecified), 10f, 12f, 1, 1, 18f },
                    { 3, 6f, new DateTime(2019, 5, 7, 0, 2, 0, 0, DateTimeKind.Unspecified), 11f, 4f, 1, 1, 15f },
                    { 2, 4f, new DateTime(2019, 5, 7, 0, 1, 0, 0, DateTimeKind.Unspecified), 10f, 5f, 1, 1, 12f },
                    { 12, 7f, new DateTime(2019, 5, 7, 0, 11, 0, 0, DateTimeKind.Unspecified), 5f, 3f, 1, 1, 15f }
                });

            migrationBuilder.InsertData(
                table: "SensorPessoa",
                columns: new[] { "Id", "Data_Hora", "Entrada", "RecintoId", "Saida", "SensorId" },
                values: new object[,]
                {
                    { 13, new DateTime(2019, 5, 7, 0, 12, 0, 0, DateTimeKind.Unspecified), 6, 1, 2, 2 },
                    { 14, new DateTime(2019, 5, 7, 0, 13, 0, 0, DateTimeKind.Unspecified), 3, 1, 4, 2 },
                    { 15, new DateTime(2019, 5, 7, 0, 14, 0, 0, DateTimeKind.Unspecified), 9, 1, 16, 2 },
                    { 19, new DateTime(2019, 5, 7, 0, 18, 0, 0, DateTimeKind.Unspecified), 6, 1, 7, 2 },
                    { 17, new DateTime(2019, 5, 7, 0, 16, 0, 0, DateTimeKind.Unspecified), 2, 1, 2, 2 },
                    { 18, new DateTime(2019, 5, 7, 0, 17, 0, 0, DateTimeKind.Unspecified), 1, 1, 5, 2 },
                    { 12, new DateTime(2019, 5, 7, 0, 11, 0, 0, DateTimeKind.Unspecified), 6, 1, 9, 2 },
                    { 16, new DateTime(2019, 5, 7, 0, 15, 0, 0, DateTimeKind.Unspecified), 7, 1, 5, 2 },
                    { 11, new DateTime(2019, 5, 7, 0, 10, 0, 0, DateTimeKind.Unspecified), 8, 1, 9, 2 },
                    { 5, new DateTime(2019, 5, 7, 0, 4, 0, 0, DateTimeKind.Unspecified), 6, 1, 5, 1 },
                    { 9, new DateTime(2019, 5, 7, 0, 8, 0, 0, DateTimeKind.Unspecified), 8, 1, 5, 1 },
                    { 8, new DateTime(2019, 5, 7, 0, 7, 0, 0, DateTimeKind.Unspecified), 2, 1, 6, 1 },
                    { 7, new DateTime(2019, 5, 7, 0, 6, 0, 0, DateTimeKind.Unspecified), 4, 1, 4, 1 },
                    { 6, new DateTime(2019, 5, 7, 0, 5, 0, 0, DateTimeKind.Unspecified), 13, 1, 9, 1 },
                    { 4, new DateTime(2019, 5, 7, 0, 3, 0, 0, DateTimeKind.Unspecified), 2, 1, 8, 1 },
                    { 3, new DateTime(2019, 5, 7, 0, 2, 0, 0, DateTimeKind.Unspecified), 7, 1, 4, 1 },
                    { 2, new DateTime(2019, 5, 7, 0, 1, 0, 0, DateTimeKind.Unspecified), 4, 1, 5, 1 },
                    { 1, new DateTime(2019, 5, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 1, 2, 1 },
                    { 20, new DateTime(2019, 5, 7, 0, 19, 0, 0, DateTimeKind.Unspecified), 0, 1, 3, 2 },
                    { 10, new DateTime(2019, 5, 7, 0, 9, 0, 0, DateTimeKind.Unspecified), 4, 1, 8, 2 },
                    { 21, new DateTime(2019, 5, 7, 0, 20, 0, 0, DateTimeKind.Unspecified), 5, 1, 5, 2 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SensorAtmosfera");

            migrationBuilder.DropTable(
                name: "SensorPessoa");
        }
    }
}
