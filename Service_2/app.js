const express = require("express");
const logger = require("morgan");
const bodyParser = require("body-parser");

// express app
const app = express();

// enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

// Log requests to the console.
app.use(logger("dev"));

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

require("./server/routes")(app);
// Setup a default catch-all route that sends back a welcome message in JSON format.
// app.get("*", (req, res) =>
//   res.status(200).send({
//     message: "Bem-vindo ao service 2."
//   })
// );

module.exports = app;
