import { TestBed } from '@angular/core/testing';

import { SensorPessoaService } from './sensor-pessoa.service';

describe('GetSensorPessoaListagemServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SensorPessoaService = TestBed.get(SensorPessoaService);
    expect(service).toBeTruthy();
  });
});
