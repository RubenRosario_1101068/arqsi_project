using System;

namespace IT1.Models
{    
    public class SensorAtmosfera
    {
        public int Id { get; set;}
        public int SensorId {get; set;}
        public int RecintoId {get; set;}
        public DateTime Data_Hora{get; set;}       
        public float Temperatura {get; set;}        
        public float Co2 {get; set;}
        public float Humidade {get; set;}
        public float Pluviosidade {get; set;}    
    }
}