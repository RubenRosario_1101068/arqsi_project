"use strict";
module.exports = (sequelize, DataTypes) => {
  const SensorPessoa = sequelize.define(
    "SensorPessoa",
    {
      id: { type: DataTypes.INTEGER, primaryKey: true },
      SensorId: { type: DataTypes.INTEGER },
      RecintoId: DataTypes.INTEGER,
      Data_Hora: DataTypes.DATE,
      Entrada: DataTypes.INTEGER,
      Saida: DataTypes.INTEGER
    },
    { freezeTableName: true, timestamps: false }
  );
  SensorPessoa.associate = function(models) {
    // associations can be defined here
  };
  return SensorPessoa;
};
