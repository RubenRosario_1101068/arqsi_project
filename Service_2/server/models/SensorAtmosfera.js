"use strict";
module.exports = (sequelize, DataTypes) => {
  const SensorAtmosfera = sequelize.define(
    "SensorAtmosfera",
    {
      id: { type: DataTypes.INTEGER, primaryKey: true },
      SensorId: DataTypes.INTEGER,
      RecintoId: DataTypes.INTEGER,
      Data_Hora: DataTypes.DATE,
      Temperatura: DataTypes.DECIMAL,
      Co2: DataTypes.DECIMAL,
      Humidade: DataTypes.DECIMAL,
      Pluviosidade: DataTypes.DECIMAL
    },
    { freezeTableName: true, timestamps: false }
  );
  SensorAtmosfera.associate = function(models) {
    // associations can be defined here
  };
  return SensorAtmosfera;
};
