using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IT1.Models;

namespace IT1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorAtmosferaController : ControllerBase
    {
        private readonly IT1Context _context;

        public SensorAtmosferaController(IT1Context context)
        {
            _context = context;
        }

        // GET: api/SensorAtmosfera/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SensorAtmosfera>> GetSensorAtmosfera(long id)
        {
            var sensorAtmosfera = await _context.SensorAtmosfera.FindAsync(id);

            if (sensorAtmosfera == null)
            {
                return NotFound();
            }

            return sensorAtmosfera;
        }

        // POST: api/SensorAtmosfera
        [HttpPost]
        public async Task<ActionResult<SensorAtmosfera>> PostSensorAtmosfera(SensorAtmosfera item)
        {
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

            _context.SensorAtmosfera.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetSensorAtmosfera), new { id = item.Id, sensorid = item.SensorId, recintoid=item.RecintoId, data_hora=item.Data_Hora, temperatura=item.Temperatura, co2=item.Co2, humidade=item.Humidade, pluviosidade=item.Pluviosidade }, item);
            }
        
        }

        // PUT: api/SensorAtmosfera/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSensorAtmosfera(long id, SensorAtmosfera item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
         
             try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SensorAtmosferaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetSensorAtmosfera), new { id = item.Id, sensorid = item.SensorId, recintoid=item.RecintoId, data_hora=item.Data_Hora, temperatura=item.Temperatura, co2=item.Co2, humidade=item.Humidade, pluviosidade=item.Pluviosidade }, item);
        }

        private bool SensorAtmosferaExists(long id)
        {
            return _context.SensorAtmosfera.Any(e => e.Id == id);
        }

        // DELETE: api/SensorAtmosfera/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSensorAtmosfera([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sensoratmosfera = await _context.SensorAtmosfera.FindAsync(id);
            if (sensoratmosfera == null)
            {
                return NotFound();
            }

            _context.SensorAtmosfera.Remove(sensoratmosfera);
            await _context.SaveChangesAsync();

            return Ok(sensoratmosfera);
        }
    }
}