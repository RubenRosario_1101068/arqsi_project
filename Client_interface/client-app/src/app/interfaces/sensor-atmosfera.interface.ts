export interface SensorAtmosfera {
  id?: number;
  SensorId: number;
  RecintoId: number;
  Data_Hora: Date;
  Temperatura: number;
  Co2: number;
  Humidade: number;
  Pluviosidade: number;
}
