const SensorAtmosfera = require("../models/").SensorAtmosfera;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

module.exports = {
  list(req, res) {
    return SensorAtmosfera.findAll()
      .then(sensoresAtmosfera => res.status(200).send(sensoresAtmosfera))
      .catch(error => res.status(400).send(error));
  },
  getRegistoById(req, res) {
    return SensorAtmosfera.findAll({
      where: {
        Id: +req.params.RegistoId
      }
    })
      .then(SensorAtmosfera => {
        if (!SensorAtmosfera) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(SensorAtmosfera);
      })
      .catch(error => res.status(400).send(error));
  },
  getById(req, res) {
    return SensorAtmosfera.findAll({
      where: {
        SensorId: +req.params.SensorId
      }
    })
      .then(sensorAtmosfera => {
        if (!sensorAtmosfera) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorAtmosfera);
      })
      .catch(error => res.status(400).send(error));
  },
  getTemperaturaInferior(req, res) {
    return SensorAtmosfera.findAll({
      where: {
        Temperatura: { [Op.lte]: req.params.valor }
      }
    })
      .then(sensorAtmosfera => {
        if (!sensorAtmosfera) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorAtmosfera);
      })
      .catch(error => res.status(400).send(error));
  },
  getTemperaturaSuperior(req, res) {
    return SensorAtmosfera.findAll({
      where: {
        Temperatura: { [Op.gte]: req.params.valor }
      }
    })
      .then(sensorAtmosfera => {
        if (!sensorAtmosfera) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorAtmosfera);
      })
      .catch(error => res.status(400).send(error));
  }
  //   create(req, res) {
  //     return SensorPessoa.create({
  //       SensorId: req.body.SensorId,
  //       RecintoId: req.body.RecintoId,
  //       Data_Hora: req.body.Data_Hora,
  //       Entrada: req.body.Entrada,
  //       Saida: req.body.Saida
  //     })
  //       .then(sensoresPessoa => res.status(201).send(sensoresPessoa))
  //       .catch(error => res.status(400).send(error));
  //   }
};
