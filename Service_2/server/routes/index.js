const sensorPessoaController = require("../controllers").sensorPessoaController;
const sensorAtmosferaController = require("../controllers")
  .sensorAtmosferaController;
const authenticator = require("../auth");
const express = require("express");
app = express();

module.exports = app => {
  app.get("/api", (req, res) =>
    res.status(200).send({
      message: "bem-vindo ao serice 2 API"
    })
  );

  /* Autenticação */
  //app.post("/api/login", (req, res) => {});

  /* routes para fazer get de registos */
  app.get(
    "/api/sensorPessoa/registo/:RegistoId",
    sensorPessoaController.getRegistoById
  );

  app.get(
    "/api/sensorAtmosfera/registo/:RegistoId",
    sensorAtmosferaController.getRegistoById
  );

  /* Routes para SensorPessoa */
  app.get("/api/sensorPessoa", sensorPessoaController.list);
  app.get("/api/sensorPessoa/:SensorId", sensorPessoaController.getById);
  app.get(
    "/api/sensorPessoa/entrada_superior/:valor",
    sensorPessoaController.getEntradaSuperior
  );
  app.get(
    "/api/sensorPessoa/entrada_inferior/:valor",
    sensorPessoaController.getEntradaInferior
  );
  app.get(
    "/api/sensorPessoa/data_anterior/:valor",
    sensorPessoaController.getDataAnterior
  );
  app.get(
    "/api/sensorPessoa/data_superior/:valor",
    sensorPessoaController.getDataSuperior
  );

  /* Routes para SensorAtmosfera */
  app.get("/api/sensorAtmosfera", sensorAtmosferaController.list);
  app.get("/api/sensorAtmosfera/:SensorId", sensorAtmosferaController.getById);
  app.get(
    "/api/sensorAtmosfera/temperatura_inferior/:valor",
    sensorAtmosferaController.getTemperaturaInferior
  );
  app.get(
    "/api/sensorAtmosfera/temperatura_superior/:valor",
    sensorAtmosferaController.getTemperaturaSuperior
  );
};
