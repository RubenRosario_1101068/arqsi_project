import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorAtmosferaComponent } from './sensor-atmosfera.component';

describe('SensorAtmosferaComponent', () => {
  let component: SensorAtmosferaComponent;
  let fixture: ComponentFixture<SensorAtmosferaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorAtmosferaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorAtmosferaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
