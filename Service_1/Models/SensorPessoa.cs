using System;

namespace IT1.Models
{
    public class SensorPessoa
    {
        public int Id { get; set;}
        public int SensorId {get; set;}
        public int RecintoId {get; set;}
        public DateTime Data_Hora{get; set;} 
        public int Entrada {get; set;}        
        public int Saida {get; set;}
    }
}