import { TestBed } from '@angular/core/testing';

import { SensorAtmosferaService } from './sensor-atmosfera.service';

describe('SensorAtmosferaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SensorAtmosferaService = TestBed.get(SensorAtmosferaService);
    expect(service).toBeTruthy();
  });
});
