using Microsoft.EntityFrameworkCore;

namespace IT1.Models
{
    public class IT1Context : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        optionsBuilder.UseSqlServer("Server=arqsi2019.database.windows.net;Database=Arqsi;User ID=AdminArqsi;Password=Adminespecial2019");
        }
        public IT1Context(DbContextOptions<IT1Context> options) : base(options)
        {
        }
        
        public DbSet<SensorPessoa> SensorPessoa{ get; set; }  
        public DbSet<SensorAtmosfera> SensorAtmosfera{ get; set; }   

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //carregar dados SensorAtmosfera
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 1, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,00,00), Temperatura = 10, Co2 = 3, Humidade = 9, Pluviosidade = 6  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 2, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,01,00), Temperatura = 12, Co2 = 4, Humidade = 10, Pluviosidade = 5  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 3, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,02,00), Temperatura = 15, Co2 = 6, Humidade = 11, Pluviosidade = 4  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 4, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,03,00), Temperatura = 18, Co2 = 4, Humidade = 10, Pluviosidade = 12  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 5, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,04,00), Temperatura = 20, Co2 = 4, Humidade = 9, Pluviosidade = 3  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 6, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,05,00), Temperatura = 9, Co2 = 5, Humidade = 9, Pluviosidade = 4  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 7, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,06,00), Temperatura = 7, Co2 = 5, Humidade = 8, Pluviosidade = 5  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 8, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,07,00), Temperatura = 6, Co2 = 9, Humidade = 7, Pluviosidade = 4  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 9, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,08,00), Temperatura = 7, Co2 = 8, Humidade = 4, Pluviosidade = 6  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 10, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,09,00), Temperatura = 10, Co2 = 8, Humidade = 2, Pluviosidade = 5  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 11, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,10,00), Temperatura = 11, Co2 = 7, Humidade = 1, Pluviosidade = 8  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 12, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,11,00), Temperatura = 15, Co2 = 7, Humidade = 5, Pluviosidade = 3  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 13, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,12,00), Temperatura = 10, Co2 = 1, Humidade = 9, Pluviosidade = 6  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 14, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,13,00), Temperatura = 10, Co2 = 4, Humidade = 7, Pluviosidade = 5  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 15, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,14,00), Temperatura = 10, Co2 = 3, Humidade = 6, Pluviosidade = 23  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 16, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,15,00), Temperatura = 9, Co2 = 2, Humidade = 7, Pluviosidade = 0  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 17, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,16,00), Temperatura = 8, Co2 = 2, Humidade = 9, Pluviosidade = 5  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 18, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,17,00), Temperatura = 7, Co2 = 4, Humidade = 8, Pluviosidade = 0  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 19, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,18,00), Temperatura = 6, Co2 = 3, Humidade = 5, Pluviosidade = 4 });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 20, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,19,00), Temperatura = 5, Co2 = 4, Humidade = 10, Pluviosidade = 6  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 21, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,20,00), Temperatura = 5, Co2 = 2, Humidade = 11, Pluviosidade = 3  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 22, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,21,00), Temperatura = 5, Co2 = 4, Humidade = 15, Pluviosidade = 4  });
            modelBuilder.Entity<SensorAtmosfera>().HasData(new SensorAtmosfera { Id = 23, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,22,00), Temperatura = 4, Co2 = 3, Humidade = 5, Pluviosidade = 6  });

            //carregar dados SensorPessoa
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 1, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,00,00), Entrada = 1, Saida = 2  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 2, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,01,00), Entrada = 4, Saida = 5  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 3, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,02,00), Entrada = 7, Saida = 4  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 4, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,03,00), Entrada = 2, Saida = 8  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 5, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,04,00), Entrada = 6, Saida = 5  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 6, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,05,00), Entrada = 13, Saida =9  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 7, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,06,00), Entrada = 4, Saida = 4  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 8, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,07,00), Entrada = 2, Saida = 6  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 9, SensorId = 1, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,08,00), Entrada = 8, Saida = 5  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 10, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,09,00), Entrada = 4, Saida = 8  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 11, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,10,00), Entrada = 8, Saida = 9  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 12, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,11,00), Entrada = 6, Saida = 9  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 13, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,12,00), Entrada = 6, Saida = 2  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 14, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,13,00), Entrada = 3, Saida = 4  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 15, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,14,00), Entrada = 9, Saida = 16  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 16, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,15,00), Entrada = 7, Saida = 5  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 17, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,16,00), Entrada = 2, Saida = 2  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 18, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,17,00), Entrada = 1, Saida = 5  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 19, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,18,00), Entrada = 6, Saida = 7  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 20, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,19,00), Entrada = 0, Saida = 3  });
            modelBuilder.Entity<SensorPessoa>().HasData(new SensorPessoa { Id = 21, SensorId = 2, RecintoId = 1, Data_Hora = new System.DateTime (2019,05,07,00,20,00), Entrada = 5, Saida = 5  });

        }
    }
}