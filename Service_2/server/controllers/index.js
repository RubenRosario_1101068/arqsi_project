const sensorPessoaController = require("./sensorPessoaController");
const sensorAtmosferaController = require("./sensorAtmosferaController");

module.exports = {
  sensorPessoaController,
  sensorAtmosferaController
};
