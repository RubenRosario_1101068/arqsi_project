const SensorPessoa = require("../models/").SensorPessoa;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment"); // operações com datas

module.exports = {
  list(req, res) {
    return SensorPessoa.findAll()
      .then(sensoresPessoa => res.status(200).send(sensoresPessoa))
      .catch(error => res.status(400).send(error));
  },
  getRegistoById(req, res) {
    return SensorPessoa.findAll({
      where: {
        Id: +req.params.RegistoId
      }
    })
      .then(sensorPessoa => {
        if (!sensorPessoa) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorPessoa);
      })
      .catch(error => res.status(400).send(error));
  },
  getById(req, res) {
    return SensorPessoa.findAll({
      where: {
        SensorId: +req.params.SensorId
      }
    })
      .then(sensorPessoa => {
        if (!sensorPessoa) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorPessoa);
      })
      .catch(error => res.status(400).send(error));
  },
  getEntradaSuperior(req, res) {
    return SensorPessoa.findAll({
      where: {
        Entrada: { [Op.gte]: req.params.valor }
      }
    })
      .then(sensorPessoa => {
        if (!sensorPessoa) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorPessoa);
      })
      .catch(error => res.status(400).send(error));
  },
  getEntradaInferior(req, res) {
    return SensorPessoa.findAll({
      where: {
        Entrada: { [Op.lte]: req.params.valor }
      }
    })
      .then(sensorPessoa => {
        if (!sensorPessoa) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorPessoa);
      })
      .catch(error => res.status(400).send(error));
  },
  getDataAnterior(req, res) {
    const dataHora = moment.parseZone(req.params.valor, "YYYY-MM-DD HH:mm:ss");
    return SensorPessoa.findAll({
      where: {
        Data_Hora: { [Op.lte]: dataHora }
      }
    })
      .then(sensorPessoa => {
        if (!sensorPessoa) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorPessoa);
      })
      .catch(error => res.status(400).send(error));
  },
  getDataSuperior(req, res) {
    const dataHora = moment.parseZone(req.params.valor, "YYYY-MM-DD HH:mm:ss");
    return SensorPessoa.findAll({
      where: {
        Data_Hora: { [Op.gte]: dataHora }
      }
    })
      .then(sensorPessoa => {
        if (!sensorPessoa) {
          return res.status(404).send({
            message: "Sensor não encontrado"
          });
        }
        return res.status(200).send(sensorPessoa);
      })
      .catch(error => res.status(400).send(error));
  }
};
