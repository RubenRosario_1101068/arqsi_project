using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IT1.Models;

namespace IT1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SensorPessoaController : ControllerBase
    {
        private readonly IT1Context _context;

        public SensorPessoaController(IT1Context context)
        {
            _context = context;
        }


        // GET: api/SensorPessoa/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SensorPessoa>> GetSensorPessoa(long id)
        {
            var sensorPessoa = await _context.SensorPessoa.FindAsync(id);

            if (sensorPessoa == null)
            {
                return NotFound();
            }

            return sensorPessoa;
        }

        [HttpPost]
        public async Task<ActionResult<SensorPessoa>> PostSensorPessoa(SensorPessoa item)
        {
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

            _context.SensorPessoa.Add(item);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetSensorPessoa), new { id = item.Id, sensorid = item.SensorId, recintoid=item.RecintoId, data_hora=item.Data_Hora, entrada=item.Entrada, SkipStatusCodePagesAttribute=item.Saida }, item);
            }
        
        }
 
        // PUT: api/SensorPessoa/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSensorPessoa(long id, SensorPessoa item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != item.Id)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;
         
             try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SensorPessoaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction(nameof(GetSensorPessoa), new { id = item.Id, sensorid = item.SensorId, recintoid=item.RecintoId, data_hora=item.Data_Hora, entrada=item.Entrada, SkipStatusCodePagesAttribute=item.Saida }, item);
        }

        private bool SensorPessoaExists(long id)
        {
            return _context.SensorPessoa.Any(e => e.Id == id);
        }

        // DELETE: api/SensorPessoa/3
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSensorPessoa([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var sensorpessoa = await _context.SensorPessoa.FindAsync(id);
            if (sensorpessoa == null)
            {
                return NotFound();
            }

            _context.SensorPessoa.Remove(sensorpessoa);
            await _context.SaveChangesAsync();

            return Ok(sensorpessoa);
        }
 
    }
}