import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from '@angular/forms';
import { SensorPessoaService } from '../services/sensor-pessoa.service';
import { SensorPessoa } from '../interfaces/sensor-pessoa.interface';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-sensor-pessoa',
  templateUrl: './sensor-pessoa.component.html',
  styleUrls: ['./sensor-pessoa.component.css']
})
export class SensorPessoaComponent implements OnInit {
  formularioPesquisa: FormGroup;
  formularioEdicao: FormGroup;
  listaSensores: SensorPessoa[];
  linhaSelecionada = -1;
  nomeColunas: string[] = [
    'id',
    'SensorId',
    'RecintoId',
    'Data_Hora',
    'Entrada',
    'Saida'
  ];

  constructor(
    private formBuilder: FormBuilder,
    private getSensorService: SensorPessoaService,
    private snackBar: MatSnackBar
  ) {
    this.listaSensores = [];
  }

  ngOnInit() {
    this.inicializarFormPesquisa();
    this.inicializarFormEdicao();
  }

  inicializarFormPesquisa() {
    this.formularioPesquisa = this.formBuilder.group({
      idRegisto: ['', [Validators.pattern('^[0-9]*$')]],
      idSensor: ['', [Validators.pattern('^[0-9]*$')]],
      dataHoraInic: new FormControl(),
      dataHoraFim: new FormControl(),
      entradas: ['', [Validators.pattern('^[0-9]*$')]],
      saidas: ['', [Validators.pattern('^[0-9]*$')]]
    });
  }

  inicializarFormEdicao() {
    this.formularioEdicao = this.formBuilder.group({
      idSensor: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      idRecinto: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      dataHora: new FormControl(),
      entradas: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
      saidas: ['', [Validators.pattern('^[0-9]*$'), Validators.required]]
    });
  }

  submit() {
    const idRegisto = this.formularioPesquisa.get('idRegisto').value;
    const idSensor = this.formularioPesquisa.get('idSensor').value;
    const dataHoraInic = this.formularioPesquisa.get('dataHoraInic').value;
    const dataHoraFim = this.formularioPesquisa.get('dataHoraFim').value;
    const entradas = this.formularioPesquisa.get('entradas').value;
    const saidas = this.formularioPesquisa.get('saidas').value;

    this.getSensorService
      .getListagemSensorPessoa(
        idRegisto,
        idSensor,
        dataHoraInic,
        dataHoraFim,
        entradas,
        saidas
      )
      .subscribe((data: SensorPessoa[]) => {
        if (data.length === 0) {
          this.snackBar.open('não existem dados', 'fechar', {
            duration: 5000,
            verticalPosition: 'top'
          });
        } else {
          this.listaSensores = data;
          this.snackBar.open(
            'foram extraído(s) ' + data.length + ' resultado(s)',
            'fechar',
            {
              duration: 5000,
              verticalPosition: 'top'
            }
          );
        }
      });
  }

  editarLinha(linha) {
    if (this.linhaSelecionada === linha.id) {
      this.linhaSelecionada = -1;
      // set valores no formulário de edição
      this.formularioEdicao.setValue({
        idSensor: '',
        idRecinto: '',
        dataHora: '',
        entradas: '',
        saidas: ''
      });
    } else {
      this.linhaSelecionada = linha.id;
      // set valores no formulário de edição
      this.formularioEdicao.setValue({
        idSensor: linha.SensorId,
        idRecinto: linha.RecintoId,
        dataHora: linha.Data_Hora,
        entradas: linha.Entrada,
        saidas: linha.Saida
      });
    }
  }

  guardarSensor() {
    let sensor: SensorPessoa;
    // Criar um novo
    if (this.linhaSelecionada === -1) {
      sensor = {
        SensorId: this.formularioEdicao.get('idSensor').value,
        RecintoId: this.formularioEdicao.get('idRecinto').value,
        Data_Hora: this.formularioEdicao.get('dataHora').value,
        Entrada: this.formularioEdicao.get('entradas').value,
        Saida: this.formularioEdicao.get('saidas').value
      };
    } else {
      // editar o existente
      sensor = {
        id: this.linhaSelecionada,
        SensorId: this.formularioEdicao.get('idSensor').value,
        RecintoId: this.formularioEdicao.get('idRecinto').value,
        Data_Hora: this.formularioEdicao.get('dataHora').value,
        Entrada: this.formularioEdicao.get('entradas').value,
        Saida: this.formularioEdicao.get('saidas').value
      };
    }

    this.getSensorService.setSensorPessoa(sensor).subscribe(data => {
      if (data) {
        this.snackBar.open('Registo guardado com sucesso', 'fechar', {
          duration: 5000,
          verticalPosition: 'top'
        });
      }
    });
  }

  eliminarLinha() {
    this.getSensorService
      .deleteSensorPessoa(this.linhaSelecionada)
      .subscribe(data => {
        if (data) {
          this.snackBar.open('Registo eliminado com sucesso', 'fechar', {
            duration: 5000,
            verticalPosition: 'top'
          });
          this.inicializarFormEdicao();
          this.linhaSelecionada = -1;
        }
      });
  }
}
